<a href="{{ $url_edit }}" class="btn btn-light btn-sm modal-show" title="Edit {{$model->nama}}"><i class="fas fa-pencil-alt"></i></a>
<a href="{{ $url_destroy }}" class="btn btn-danger btn-sm btn-delete" title="Hapus {{$model->nama}}"><i class="fas fa-trash"></i></a>
<a href="{{ $url_show }}" class="btn btn-primary btn-sm btn-show" title="Detail {{$model->nama}}"><i class="fas fa-eye"></i></a>