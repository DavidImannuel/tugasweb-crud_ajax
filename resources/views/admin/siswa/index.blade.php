@extends('layout.app')
@section('title','Daftar Siswa')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
	        <div class="card-body">
	            <a href="{{ route('siswa.create') }}" class="btn btn-success modal-show" title="Tambah Guru">
				  <i class="fas fa-plus"></i> Tambah Siswa
				</a>
	        </div>
	        <table class="table" id="siswa">
	              <thead>
	                <tr>
		              <th>No</th>
	                  <th>Nama</th>
	                  <th>Telepon</th>
	                  <th>Jurusan</th>
	                  <th>Kelamin</th>
	                  <th>Opsi</th>
	                </tr>
	              </thead>
	              <tbody>
	              </tbody>
	        </table>
	    </div>
	</div>
</div>
@include('layout.modal')
@include('layout.modal_large')
@endsection

@push('scripts')
	@include('admin.siswa.js')
@endpush