<form action="{{ (isset($model) ? route('siswa.update',$model->id): route('siswa.store') ) }}" method="post" enctype="multipart/form-data">
  @csrf
  @if(isset($model))
    @method('PUT')
  @else
    @method('POST')
  @endif
  <div class="form-group">
    <label>Nama</label>
    <input required type="text" name="nama" id="nama" class="form-control" autocomplete="off" @isset($model) value="{{ $model->nama }}" @endisset>
  </div>
  <div class="form-group">
    <label>Jenis Kelamin</label>
    <select name="jenis_kelamin" class="form-control">
      <option value="" selected>Pilih Jenis Kelamin..</option>}
      <option @isset($model) @if($model->jenis_kelamin == 'lelaki') selected  @endif @endisset value="lelaki">Lelaki</option>
      <option @isset($model) @if($model->jenis_kelamin == 'perempuan') selected  @endif @endisset value="perempuan">Perempuan</option>
    </select>
  </div>
  <div class="form-group">
    <label>Nama</label>
    <input required type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control" autocomplete="off" @isset($model) value="{{ $model->tanggal_lahir }}" @endisset>
  </div>
  <div class="form-group">
    <label>Agama</label>
    <select name="agama" class="form-control">
      <option value="" selected>Pilih Agama..</option>}
      <option @isset($model) @if($model->agama == 'islam') selected  @endif @endisset value="islam">Islam</option>
      <option @isset($model) @if($model->agama == 'kristen') selected  @endif @endisset value="kristen">Kristen</option>
      <option @isset($model) @if($model->agama == 'katholik') selected  @endif @endisset value="katholik">Katholik</option>
      <option @isset($model) @if($model->agama == 'budha') selected  @endif @endisset value="budha">Budha</option>
      <option @isset($model) @if($model->agama == 'hindu') selected  @endif @endisset value="hindu">Hindu</option>
    </select>
  </div>
  <div class="form-group">
    <label>Email</label>
    <input type="email" name="email" id="email" class="form-control" autocomplete="off" @isset($model) value="{{ $model->email }}" @endisset>
  </div>
  <div class="form-group">
    <label>Kelas</label>
    <select name="kelas" class="form-control">
      <option value="" selected>Pilih Kelas..</option>}
      <option @isset($model) @if($model->kelas == 'X') selected  @endif @endisset value="X">X</option>
      <option @isset($model) @if($model->kelas == 'XI') selected  @endif @endisset value="XI">XI</option>
      <option @isset($model) @if($model->kelas == 'XII') selected  @endif @endisset value="XII">XII</option>
    </select>
  </div>
  <div class="form-group">
    <label>Jurusan</label>
    <select name="jurusan" class="form-control">
      <option value="" selected>Pilih Jurusan..</option>}
      <option @isset($model) @if($model->jurusan == 'RPL') selected  @endif @endisset value="RPL">RPL</option>
      <option @isset($model) @if($model->jurusan == 'TKJ') selected  @endif @endisset value="TKJ">TKJ</option>
      <option @isset($model) @if($model->jurusan == 'SIJA') selected  @endif @endisset value="SIJA">SIJA</option>
      <option @isset($model) @if($model->jurusan == 'Mesin') selected  @endif @endisset value="Mesin">Mesin</option>
      <option @isset($model) @if($model->jurusan == 'TTL') selected  @endif @endisset value="TTL">TTL</option>
    </select>
  </div>
  <div class="form-group">
    <label>Telepon</label>
    <input required type="telepon" name="telepon" id="telepon" class="form-control" autocomplete="off" @isset($model) value="{{ $model->telepon }}" @endisset>
  </div>
  <div class="form-group">
    <label>Tentang</label>
    <textarea name="tentang" id="tentang" class="form-control" >@isset($model) {{ $model->tentang }} @endisset</textarea>
  </div>
  <div class="form-group">
    <label>Foto</label>
    <input type="file" name="file" id="file" class="form-control file">
  </div>
</form>