<div class="row">
    <div class="col-md-6">
      <div class="card" style="width: 18rem;">
        <img src="{{asset('storage/'.$model->foto)}}" class="card-img-top" >
        <div class="card-body">
          <p class="card-text"> @if (isset($model->tentang))
              {{ $model->tentang }} @else
           {{ $model->nama }} lahir pada tanggal {{ $model->tanggal_lahir }} dan sekarang berada di jurusan {{ $model->jurusan }} @endif</p>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card" style="width: 18rem;">
        <div class="card-header">
          Profil Lengkap
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item">Nama : {{ $model->nama }}</li>
          <li class="list-group-item">Tanggal Lahir : {{ $model->tanggal_lahir }}</li>
          <li class="list-group-item">Jenis Kelamin : {{ $model->jenis_kelamin }} </li>
          <li class="list-group-item">Agama : {{ $model->agama }} </li>
          <li class="list-group-item">Email : {{ $model->email }} </li>
          <li class="list-group-item">Jurusan : {{ $model->jurusan }} </li>
          <li class="list-group-item">No. Telepon : {{ $model->telepon }} </li>
        </ul>
      </div>
    </div>
  </div>