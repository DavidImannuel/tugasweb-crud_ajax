

<script>
	var table = $('#guru').DataTable({
		ajax: "{{ route('api.guru') }}",
		serverSide: true,
		processing: true,
		columns: [
			{ data: 'DT_RowIndex', name: 'id'},
			{ data: 'nama', name: 'nama' },
			{ data: 'telepon', name: 'telepon' },
			{ data: 'jurusan', name: 'jurusan' },
			{ data: 'jenis_kelamin', name: 'jenis_kelamin' },
			{ data: 'opsi', name: 'opsi' },
		],
	});

	$('body').on('click','.modal-show',function(e){
		
		e.preventDefault();
		var me = $(this),
			url = me.attr('href'),
			title = me.attr('title');

		$('#modal-title').text(title);
		$('#modal-btn-save').text('Simpan');

		$.ajax({
			url: url,
			dataType: 'html',
			success: function(response){
				$('#modal-body').html(response);
			}
		});
		
		$('#modal').modal('show');
	});

	$('#modal-btn-save').click(function (e) {
		// e.preventDefault();

		var form = $('#modal-body form'),
			url = form.attr('action'),
			method = $('input[name=_method]').val();
			data = new FormData(form[0]);
			data.append('_method', method);
			
		$.ajax({
			url: url,
			method: 'POST',
			data:data,
			processData: false,
			contentType: false,
			success: function (response) {
				// console.log(response);
				swal("Sukses!", "Data Berhasil Disimpan", "success");
				$('#modal').modal('hide');
				table.ajax.reload();
			},
			error: function (xhr) {
				errors = xhr.responseJSON.errors;

				$.each(errors,function (i,error) {
					console.log(i+":"+error);
					toastr.error(error, 'Error!!');
				});
			}
		});

	});

	$('body').on('click','.btn-delete',function(e){
			e.preventDefault();
			var me = $(this),
				url = me.attr('href');

			swal({
			  title: "Apakah Kamu Yakin?",
			  text: "Data akan dihapus!",
			  icon: "warning",
			  buttons: true,
			  dangerMode: true,
			})
			.then((willDelete) => {
			  if (willDelete) {
			  	$.ajax({
					url:  url,
					method: 'POST',
					data: {
						_method: 'DELETE',
						_token: '{{ csrf_token() }}', 
					},
					success: function(response){
						// console.log(response);
						 swal("Data sudah dihapus", {
					      icon: "success",
					    });
						table.ajax.reload();

					},
					error: function(xhr){
						console.log(xhr);
					}
				});

			  } else {
			    swal("Data gagal dihapus");
			  }
			});
		});

	$('body').on('click','.btn-show',function(e){
		e.preventDefault();
		var me = $(this),
				url = me.attr('href'),
				title = me.attr('title');
		
		$('.modal-large-title').text(title);

		$.ajax({
			url: url,
			dataType: 'html',
			success: function(response){
				console.log(response);
				$('#modal-large-body').html(response);
			},
		});
		$('#modal-large').modal('show');
	});

</script>