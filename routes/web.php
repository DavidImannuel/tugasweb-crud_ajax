<?php

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'],function(){

	Route::get('/',function () {
		return view('admin.dashboard');
	});

	Route::resource('/guru','Admin\Guru\GuruController');
	Route::resource('/siswa','Admin\Siswa\SiswaController');
});
//API
Route::get('guru-api','Admin\Guru\GuruController@Api')->name('api.guru');
Route::get('siswa-api','Admin\Siswa\SiswaController@Api')->name('api.siswa');
