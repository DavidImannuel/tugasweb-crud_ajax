<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama',100);
            $table->string('jenis_kelamin',10);
            $table->date('tanggal_lahir');
            $table->string('agama',20);
            $table->string('email',100)->nullable();
            $table->string('jurusan',100);
            $table->string('telepon',50);
            $table->string('foto',100)->nullable();
            $table->enum('kelas', ['X','XI', 'XII']);
            $table->text('tentang')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
