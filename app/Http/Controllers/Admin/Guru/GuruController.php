<?php

namespace App\Http\Controllers\Admin\Guru;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Guru;
use Illuminate\Support\Facades\Storage;
use DataTables;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Guru::all();
        return view('admin.guru.index',compact('model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.guru.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        // dd($request->all());

        if ($request->hasFile('file')) {
            
            $request->validate([
                'nama' => 'required',
                'jenis_kelamin' => 'required',
                'agama' => 'required',
                'jurusan' => 'required',
                'telepon' => 'required',
                'email' => 'nullable|email',
                'file' => 'mimes:jpeg,bmp,png',
            ]);

            $path = $request->file->store('images');
            $request->merge([
                'foto' => $path,
            ]);
        } else {
            $request->validate([
                'nama' => 'required',
                'jenis_kelamin' => 'required',
                'agama' => 'required',
                'jurusan' => 'required',
                'telepon' => 'required',
                'email' => 'nullable|email',
            ]);
        }
        Guru::insert($request->except(['_token','_method','file']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Guru::find($id);
        return view('admin.guru.detail',compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Guru::find($id);
        return view('admin.guru.form',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->hasFile('file')) {
            
            $request->validate([
                'nama' => 'required',
                'jenis_kelamin' => 'required',
                'agama' => 'required',
                'tanggal_lahir' => 'required',
                'jurusan' => 'required',
                'telepon' => 'required',
                'email' => 'nullable|email',
                'file' => 'mimes:jpeg,bmp,png',
            ]);
            $oldFile = Guru::select('foto')->where('id',$id)->first()->foto;
            Storage::delete($oldFile);
            $path = $request->file->store('images');
            $request->merge([
                'foto' => $path,
            ]);
        } else {
            $request->validate([
                'nama' => 'required',
                'jenis_kelamin' => 'required',
                'agama' => 'required',
                'tanggal_lahir' => 'required',
                'jurusan' => 'required',
                'telepon' => 'required',
                'email' => 'nullable|email',
            ]);
        }

        Guru::where('id',$id)->update($request->except(['_token','_method','file']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $foto = Guru::select('foto')->where('id',$id)->first()->foto;
        Storage::delete($foto);
        Guru::destroy($id);
    }

    public function Api(){
        
        $model = Guru::query();

        return DataTables::of($model)
        ->addColumn('opsi', function($model){
            // dd($model);
            return view('layout._action',[
                'model' => $model,
                'url_edit' => route('guru.edit', $model->id),
                'url_destroy' => route('guru.destroy', $model->id),
                'url_show' => route('guru.show', $model->id),
            ]);
        })
        ->editColumn('nama', function($model){
            return ucwords($model->nama);
        })
        ->rawColumns(['opsi'])
        ->addIndexColumn()
        ->toJson();
    }
}
